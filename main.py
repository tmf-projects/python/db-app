from tkinter import *
from tkinter import ttk
from mysql.connector import Error, MySQLConnection
from modules import python_mysql_dbconfig


class MainWin:
    def __init__(self, window: Tk):
        self.win = window
        self.style = ttk.Style()
        self.style.configure("Bold.TButton", font='TkDefaultFont 14 bold')

        self.menu = Menu(self.win)

        self.win.config(menu=self.menu)

        self.file_menu = Menu(self.menu, tearoff=0)
        self.new_menu = Menu(self.file_menu, tearoff=0)
        self.open_menu = Menu(self.file_menu, tearoff=0)
        self.help_menu = Menu(self.menu, tearoff=0)

        self.tab_control = ttk.Notebook(self.win)
        self.tab_menu = Menu(self.menu, tearoff=0)

        self.status_bar = Label(self.win, font='TkDefaultFont', bd=1, relief=SUNKEN, anchor=W)

        self.init()

    def init(self):
        self.init_menu()
        self.init_widgets()

    def init_menu(self):
        self.new_menu.add_command(label="brand",
                                  command=lambda: InputTable(self.tab_control, "brand", "_create"))
        self.new_menu.add_command(label="phone",
                                  command=lambda: InputTable(self.tab_control, "phone", "_create"))
        self.new_menu.add_command(label="client_order",
                                  command=lambda: InputTable(self.tab_control, "client_order", "_create"))
        self.new_menu.add_command(label="client",
                                  command=lambda: InputTable(self.tab_control, "client", "_create"))

        self.open_menu.add_command(label="brand",
                                   command=lambda: OutputTable(self.tab_control, "brand"))
        self.open_menu.add_command(label="phone",
                                   command=lambda: OutputTable(self.tab_control, "phone"))
        self.open_menu.add_command(label="client_order",
                                   command=lambda: OutputTable(self.tab_control, "client_order"))
        self.open_menu.add_command(label="client",
                                   command=lambda: OutputTable(self.tab_control, "client"))

        self.file_menu.add_cascade(label="Добавить информацию в таблицу...", menu=self.new_menu)
        self.file_menu.add_separator()
        self.file_menu.add_cascade(label="Открыть таблицу...", menu=self.open_menu)
        self.menu.add_cascade(label="База Данных", menu=self.file_menu)

        self.tab_menu.add_command(label="Закрыть текущую вкладку", command=self.close_select_tab,
                                  accelerator="Delete")
        self.tab_menu.add_command(label="Закрыть все вкладки", command=self.close_all_tabs,
                                  accelerator="Alt+Delete")
        self.tab_menu.add_separator()
        self.tab_menu.add_command(label="Начальная вкладка",
                                  command=lambda: WelcomeTable(main_app.tab_control, "Welcome"),
                                  accelerator="Home")
        self.tab_control.bind("<Button-3>", lambda event: self.tab_menu.post(event.x_root, event.y_root))
        self.tab_control.bind("<Delete>", self.close_select_tab_event)
        self.tab_control.bind("<Alt_L><Delete>", self.close_all_tabs_event)
        self.tab_control.bind("<Home>", self.event_home)
        # self.menu.add_cascade(label="Вкладки", menu=self.tab_menu)

        self.help_menu.add_command(label="Помощь", command=lambda: HelpTable(main_app.tab_control, "Help"))
        self.help_menu.add_command(label="О программе")
        self.menu.add_cascade(label="Справка", menu=self.help_menu)

    def init_widgets(self):
        self.tab_control.pack(expand=1, fill='both')
        self.status_bar.pack(side=BOTTOM, fill=X)

    def close_select_tab_event(self, event):
        self.close_select_tab()

    def close_select_tab(self):
        for item in self.tab_control.winfo_children():
            if str(item) == self.tab_control.select():
                item.destroy()
                return

    def close_all_tabs_event(self, event):
        self.close_all_tabs()

    def close_all_tabs(self):
        for item in self.tab_control.winfo_children():
            item.destroy()

    def event_home(self, event):
        WelcomeTable(main_app.tab_control, "Welcome")


class Table(ttk.Frame):
    def __init__(self, tab, first_name="", second_name=""):
        super().__init__(tab)

        self.create(first_name)

        tab.add(self, text=str(first_name) + second_name)
        tab.select(tab.winfo_children()[-1])

    def create(self, first_name):
        pass

    def init(self, first_name):
        pass


class WelcomeTable(Table):
    def create(self, first_name):
        label_welcome = Label(self, text="Добро Пожаловать в DMobile!", font='TkDefaultFont 14 bold', fg='#0a9df2')
        label_open_info = Label(self, text="Открыть таблицу...", font="TkDefaultFont 14 bold")

        btn_open_brand = ttk.Button(self, text="brand", style='Bold.TButton',
                                    command=lambda: OutputTable(main_app.tab_control, "brand"))
        btn_open_phone = ttk.Button(self, text="phone", style='Bold.TButton',
                                    command=lambda: OutputTable(main_app.tab_control, "phone"))
        btn_open_client_order = ttk.Button(self, text="client_order", style='Bold.TButton',
                                           command=lambda: OutputTable(main_app.tab_control, "client_order"))
        btn_open_client = ttk.Button(self, text="client", style='Bold.TButton',
                                     command=lambda: OutputTable(main_app.tab_control, "client"))

        label_new_info = Label(self, text="Добавить информацию в таблицу...", font="TkDefaultFont 14 bold")

        btn_new_brand = ttk.Button(self, text="brand", style='Bold.TButton',
                                   command=lambda: InputTable(main_app.tab_control, "brand", "_create"))
        btn_new_phone = ttk.Button(self, text="phone", style='Bold.TButton',
                                   command=lambda: InputTable(main_app.tab_control, "phone", "_create"))
        btn_new_client_order = ttk.Button(self, text="client_order", style='Bold.TButton',
                                          command=lambda: InputTable(main_app.tab_control, "client_order", "_create"))
        btn_new_client = ttk.Button(self, text="client", style='Bold.TButton',
                                    command=lambda: InputTable(main_app.tab_control, "client", "_create"))

        label_info = Label(self, text="Также имеется...", font="TkDefaultFont 14 bold")
        btn_help = ttk.Button(self, text="Помощь", style='Bold.TButton', command=lambda: HelpTable(main_app.tab_control, "Help"))

        label_welcome.grid(row=0, columnspan=5)

        label_open_info.grid(row=1, columnspan=5, pady=5)

        btn_open_brand.grid(row=2, column=0, sticky="ew", padx=30, ipady=10)
        btn_open_phone.grid(row=2, column=1, sticky="ew", padx=30, ipady=10)
        btn_open_client_order.grid(row=2, column=2, sticky="ew", padx=30, ipady=10)
        btn_open_client.grid(row=2, column=3, sticky="ew", padx=30, ipady=10)

        label_new_info.grid(row=3, columnspan=5, pady=5)

        btn_new_brand.grid(row=4, column=0, sticky="ew", padx=30, ipady=10)
        btn_new_phone.grid(row=4, column=1, sticky="ew", padx=30, ipady=10)
        btn_new_client_order.grid(row=4, column=2, sticky="ew", padx=30, ipady=10)
        btn_new_client.grid(row=4, column=3, sticky="ew", padx=30, ipady=10)

        label_info.grid(row=5, columnspan=5, pady=5)

        btn_help.grid(row=6, columnspan=5, sticky="ew", padx=30, ipady=10)

        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure(2, weight=1)
        self.grid_columnconfigure(3, weight=1)


class HelpTable(Table):
    def create(self, first_name):
        label_faq = Label(self, text="FAQ (ЧаВо):", font='TkDefaultFont 14 bold', fg='#0a9df2')
        label_1q = Label(self, text="В. Программа зависает (либо ничего не делает) "
                                    "при попытке удалении одного из брендов или клиентов.",
                         font='TkDefaultFont 12')
        label_1a = Label(self, text="О. Вы не можете удалить внешние ключи, пока существует зависимость от них.",
                         font='TkDefaultFont 12')
        label_2q = Label(self, text="В. Как закрыть вкладку?",
                         font='TkDefaultFont 12')
        label_2a = Label(self, text="О. Сначала нажмите на вкладку, которую хотите закрыть,\n"
                                    "нажмите на клавишу \"Delete\" (или ПКМ > \"Закрыть текущую вкладку\").",
                         font='TkDefaultFont 12')

        label_faq.grid(row=0, column=0)

        label_1q.grid(row=1, column=0)
        label_1a.grid(row=2, column=0)
        label_2q.grid(row=3, column=0)
        label_2a.grid(row=4, column=0)

        self.grid_columnconfigure(0, weight=1)


class InputTable(Table):
    def create(self, first_name):
        self.label_brand_name = Label(self, text="NAME", font='TkDefaultFont 14 bold')
        self.brand_name = Label(self, font='TkDefaultFont 14 bold')
        self.entry_brand_name = ttk.Entry(self, font='TkDefaultFont 14 bold')

        self.label_phone_name = Label(self, text="NAME", font='TkDefaultFont 14 bold')
        self.phone_name = Label(self, font='TkDefaultFont 14 bold')
        self.label_phone_brand_id = Label(self, text="BRAND_ID", font='TkDefaultFont 14 bold')
        self.label_phone_brand_name = Label(self, font='TkDefaultFont 14 bold')
        self.label_phone_creation_year = Label(self, text="CREATION_YEAR", font='TkDefaultFont 14 bold')
        self.label_phone_quantity = Label(self, text="QUANTITY", font='TkDefaultFont 14 bold')
        self.label_phone_price = Label(self, text="PRICE", font='TkDefaultFont 14 bold')
        self.entry_phone_name = ttk.Entry(self, font='TkDefaultFont 14 bold')
        self.entry_phone_brand_id = ttk.Entry(self, font='TkDefaultFont 14 bold')
        self.entry_phone_creation_year = ttk.Entry(self, font='TkDefaultFont 14 bold')
        self.entry_phone_quantity = ttk.Entry(self, font='TkDefaultFont 14 bold')
        self.entry_phone_price = ttk.Entry(self, font='TkDefaultFont 14 bold')

        self.label_client_order_client_id = Label(self, text="CLIENT_ID", font='TkDefaultFont 14 bold')
        self.label_client_order_phone_id = Label(self, text="PHONE_ID", font='TkDefaultFont 14 bold')
        self.label_client_order_price = Label(self, text="PRICE = ", font='TkDefaultFont 14 bold')
        self.label_client_order_phone_name = Label(self, font='TkDefaultFont 14 bold')
        self.label_client_order_client_name = Label(self, font='TkDefaultFont 14 bold')
        self.client_order_price = Label(self, text="0", font='TkDefaultFont 14 bold')
        self.entry_client_order_phone_id = ttk.Entry(self, font='TkDefaultFont 14 bold')
        self.entry_client_order_client_id = ttk.Entry(self, font='TkDefaultFont 14 bold')

        self.label_client_name = Label(self, text="NAME", font='TkDefaultFont 14 bold')
        self.entry_client_name = ttk.Entry(self, font='TkDefaultFont 14 bold')

        if first_name == "brand":
            self.label_brand_name.grid(row=0, column=0)
            self.brand_name.grid(row=0, column=2)

            self.entry_brand_name.grid(row=0, column=1)
            self.entry_brand_name.bind('<KeyRelease>', self.check_brand_name)

            btn_apply = ttk.Button(self, text="Добавить", style='Bold.TButton', command=lambda: self.init(first_name))
            btn_apply.grid(row=1, column=0)
        elif first_name == "phone":
            # self.label_phone_img = Label(self, text="IMG", font=('TkDefaultFont 14 bold'))

            self.label_phone_name.grid(row=0, column=0)
            self.phone_name.grid(row=0, column=2)
            self.label_phone_brand_id.grid(row=1, column=0)
            self.label_phone_brand_name.grid(row=1, column=2)
            self.label_phone_creation_year.grid(row=2, column=0)
            self.label_phone_quantity.grid(row=3, column=0)
            self.label_phone_price.grid(row=4, column=0)
            # self.label_phone_img.grid(row=5, column=0)

            # self.img_phone_img = ttk.Entry(self, font=('TkDefaultFont 14 bold'))

            self.entry_phone_brand_id.bind('<KeyRelease>', self.check_brand)
            self.entry_phone_name.bind('<KeyRelease>', self.check_phone_name)

            self.entry_phone_name.grid(row=0, column=1)
            self.entry_phone_brand_id.grid(row=1, column=1)
            self.entry_phone_creation_year.grid(row=2, column=1)
            self.entry_phone_quantity.grid(row=3, column=1)
            self.entry_phone_price.grid(row=4, column=1)
            # self.img_phone_img.grid(row=5, column=1)

            btn_apply = ttk.Button(self, text="Добавить", style='Bold.TButton', command=lambda: self.init(first_name))
            btn_apply.grid(row=5, column=0)
        elif first_name == "client_order":

            self.label_client_order_client_id.grid(row=0, column=0)
            self.label_client_order_client_name.grid(row=0, column=2)
            self.label_client_order_phone_id.grid(row=1, column=0)
            self.label_client_order_phone_name.grid(row=1, column=2)
            self.label_client_order_price.grid(row=2, column=0)
            self.client_order_price.grid(row=2, column=1)

            self.entry_client_order_client_id.bind('<KeyRelease>', self.check_client_name)
            self.entry_client_order_phone_id.bind('<KeyRelease>', self.check_phone)

            self.entry_client_order_client_id.grid(row=0, column=1)
            self.entry_client_order_phone_id.grid(row=1, column=1)

            btn_apply = ttk.Button(self, text="Добавить", style='Bold.TButton', command=lambda: self.init(first_name))
            btn_apply.grid(row=3, column=0)
        elif first_name == "client":
            self.label_client_name.grid(row=0, column=0)

            self.entry_client_name.grid(row=0, column=1)

            btn_apply = ttk.Button(self, text="Добавить", style='Bold.TButton', command=lambda: self.init(first_name))
            btn_apply.grid(row=1, column=0)

    def init(self, first_name):
        if first_name == "brand":
            if db.check_brand(self.entry_brand_name.get(), self.brand_name):
                db.insert_brand(self.entry_brand_name.get())
        elif first_name == "phone":
            if db.check_phone(self.entry_phone_name.get(), int(self.entry_phone_brand_id.get()), self.brand_name):
                db.insert_phone(self.entry_phone_name.get(), int(self.entry_phone_brand_id.get()),
                                int(self.entry_phone_creation_year.get()), int(self.entry_phone_quantity.get()),
                                float(self.entry_phone_price.get()))
            # input_list.append(self.entry_phone_img['text'])
        elif first_name == "client_order":
            if db.get_phone_price_quantity(self.entry_client_order_phone_id.get(), self.client_order_price):
                db.insert_client_order(int(self.entry_client_order_client_id.get()),
                                       int(self.entry_client_order_phone_id.get()),
                                       float(self.client_order_price['text']))
                db.buy_phone(int(self.entry_client_order_phone_id.get()))
            root.after(100, db.get_phone_price_quantity,
                       self.entry_client_order_phone_id.get(), self.client_order_price)
            root.after(100, db.get_phone_full_name,
                       self.entry_client_order_phone_id.get(), self.label_client_order_phone_name)
        elif first_name == "client":
            db.insert_client(self.entry_client_name.get())

    @staticmethod
    def check_id(event):
        if event.keysym == '1' \
                or event.keysym == '2' \
                or event.keysym == '3' \
                or event.keysym == '4' \
                or event.keysym == '5' \
                or event.keysym == '6' \
                or event.keysym == '7' \
                or event.keysym == '8' \
                or event.keysym == '9' \
                or event.keysym == '0' \
                or event.keysym == 'BackSpace':
            return True
        else:
            return False

    def check_brand(self, event):
        if self.check_id(event):
            root.after(100, db.get_brand_name,
                       self.entry_phone_brand_id.get(), self.label_phone_brand_name)
            self.check_phone_name(event)

    def check_brand_name(self, event):
        root.after(100, db.check_brand, self.entry_brand_name.get(), self.brand_name)

    def check_phone(self, event):
        if self.check_id(event):
            root.after(100, db.get_phone_price_quantity,
                       self.entry_client_order_phone_id.get(), self.client_order_price)
            root.after(100, db.get_phone_full_name,
                       self.entry_client_order_phone_id.get(), self.label_client_order_phone_name)

    def check_phone_name(self, event):
        try:
            root.after(100, db.check_phone,
                       self.entry_phone_name.get(), int(self.entry_phone_brand_id.get()), self.phone_name)
        except ValueError as e:
            main_app.status_bar['text'] = e

    def check_client_name(self, event):
        if self.check_id(event):
            root.after(100, db.get_client_name,
                       self.entry_client_order_client_id.get(), self.label_client_order_client_name)


class OutputTable(Table):
    def create(self, first_name):
        if first_name == "brand":
            tree = ttk.Treeview(self)
            tree['show'] = 'headings'
            tree['columns'] = ("ID", "NAME")
            tree.column("ID", width=10, minwidth=10, anchor=CENTER)
            tree.column("NAME", width=30, minwidth=10, anchor=CENTER)
            tree.heading("ID", text="ID", anchor=CENTER)
            tree.heading("NAME", text="NAME", anchor=CENTER)

            i = 0
            con = db.connect()
            cur = con.cursor()
            cur.execute("SELECT * FROM brand")
            for ro in cur:
                tree.insert('', i, text='', values=(ro[0], ro[1]))
                i = i + 1

            tree.pack(expand=1, fill='both')

            context_menu = Menu(self, tearoff=0)
            context_menu.add_command(label="Изменить выделенную запись",
                                     command=lambda: self.edit_selected_entry(tree, first_name))
            context_menu.add_command(label="Удалить выделенную запись",
                                     command=lambda: self.delete_select_entry(tree, first_name))
            tree.bind("<Button-3>", lambda event: context_menu.post(event.x_root, event.y_root))
        elif first_name == "phone":
            tree = ttk.Treeview(self)
            tree['show'] = 'headings'
            tree['columns'] = ("ID", "NAME", "BRAND", "CREATION_YEAR", "QUANTITY", "PRICE")
            tree.column("ID", width=5, minwidth=5, anchor=CENTER)
            tree.column("NAME", width=30, minwidth=10, anchor=CENTER)
            tree.column("BRAND", width=20, minwidth=10, anchor=CENTER)
            tree.column("CREATION_YEAR", width=20, minwidth=10, anchor=CENTER)
            tree.column("QUANTITY", width=10, minwidth=10, anchor=CENTER)
            tree.column("PRICE", width=10, minwidth=10, anchor=CENTER)
            tree.heading("ID", text="ID", anchor=CENTER)
            tree.heading("NAME", text="NAME", anchor=CENTER)
            tree.heading("BRAND", text="BRAND", anchor=CENTER)
            tree.heading("CREATION_YEAR", text="CREATION_YEAR", anchor=CENTER)
            tree.heading("QUANTITY", text="QUANTITY", anchor=CENTER)
            tree.heading("PRICE", text="PRICE", anchor=CENTER)

            i = 0
            con = db.connect()
            cur = con.cursor()
            cur.execute("""SELECT phone.id, phone.name, brand.name AS brand,
            phone.creation_year, phone.quantity, phone.price
            FROM phone JOIN brand ON phone.brand_id = brand.id""")
            for ro in cur:
                tree.insert('', i, text='',
                            values=(ro[0], ro[1], ro[2], ro[3], ro[4], ro[5]))
                i = i + 1

            tree.pack(expand=1, fill='both')

            context_menu = Menu(self, tearoff=0)
            context_menu.add_command(label="Изменить выделенную запись",
                                     command=lambda: self.edit_selected_entry(tree, first_name))
            context_menu.add_command(label="Удалить выделенную запись",
                                     command=lambda: self.delete_select_entry(tree, first_name))
            tree.bind("<Button-3>", lambda event: context_menu.post(event.x_root, event.y_root))
        elif first_name == "client_order":
            tree = ttk.Treeview(self)
            tree['show'] = 'headings'
            tree['columns'] = ("ID", "CLIENT_NAME", "PHONE_NAME", "PRICE")
            tree.column("ID", width=5, minwidth=5, anchor=CENTER)
            tree.column("CLIENT_NAME", width=5, minwidth=5, anchor=CENTER)
            tree.column("PHONE_NAME", width=30, minwidth=10, anchor=CENTER)
            tree.column("PRICE", width=20, minwidth=10, anchor=CENTER)
            tree.heading("ID", text="ID", anchor=CENTER)
            tree.heading("CLIENT_NAME", text="CLIENT_NAME", anchor=CENTER)
            tree.heading("PHONE_NAME", text="PHONE_NAME", anchor=CENTER)
            tree.heading("PRICE", text="PRICE", anchor=CENTER)

            i = 0
            con = db.connect()
            cur = con.cursor()
            cur.execute("""SELECT client_order.id as id, client.name AS client_name,
            phone.name AS phone_name, client_order.price
            FROM client_order JOIN client JOIN phone ON client_order.client_id = client.id
            AND client_order.phone_id = phone.id""")
            for ro in cur:
                tree.insert('', i, text='', values=(ro[0], ro[1], ro[2], ro[3]))
                i = i + 1

            tree.pack(expand=1, fill='both')

            context_menu = Menu(self, tearoff=0)
            context_menu.add_command(label="Изменить выделенную запись",
                                     command=lambda: self.edit_selected_entry(tree, first_name))
            context_menu.add_command(label="Удалить выделенную запись",
                                     command=lambda: self.delete_select_entry(tree, first_name))
            tree.bind("<Button-3>", lambda event: context_menu.post(event.x_root, event.y_root))
        elif first_name == "client":
            tree = ttk.Treeview(self)
            tree['show'] = 'headings'
            tree['columns'] = ("ID", "NAME")
            tree.column("ID", width=10, minwidth=10, anchor=CENTER)
            tree.column("NAME", width=30, minwidth=10, anchor=CENTER)
            tree.heading("ID", text="ID", anchor=CENTER)
            tree.heading("NAME", text="NAME", anchor=CENTER)

            i = 0
            con = db.connect()
            cur = con.cursor()
            cur.execute("SELECT * FROM client")
            for ro in cur:
                tree.insert('', i, text='', values=(ro[0], ro[1]))
                i = i + 1

            tree.pack(expand=1, fill='both')

            context_menu = Menu(self, tearoff=0)
            context_menu.add_command(label="Изменить выделенную запись",
                                     command=lambda: self.edit_selected_entry(tree, first_name))
            context_menu.add_command(label="Удалить выделенную запись",
                                     command=lambda: self.delete_select_entry(tree, first_name))
            tree.bind("<Button-3>", lambda event: context_menu.post(event.x_root, event.y_root))

    @staticmethod
    def delete_select_entry(tree, table):
        try:
            selected_item = tree.selection()[0]
            values = tuple(tree.item(selected_item)['values'])
            print(values)
            con = db.connect()
            cur = con.cursor()
            query = f"DELETE FROM {table} WHERE id={values[0]}"
            cur.execute(query)
            con.commit()
            tree.delete(selected_item)
        except Error as e:
            main_app.status_bar['text'] = e

    @staticmethod
    def edit_selected_entry(tree, table):
        selected_item = tree.selection()[0]
        values = tuple(tree.item(selected_item)['values'])
        print(values)
        con = db.connect()
        cur = con.cursor()
        query = f"SELECT * FROM {table} WHERE id={values[0]}"
        cur.execute(query)
        values = cur.fetchone()
        print(values)
        EditTable(main_app.tab_control, values, table, "_edit")


class EditTable(InputTable):
    def __init__(self, tab, values, first_name="", second_name=""):
        super().__init__(tab)
        self.values = values

        self.create(first_name)

        tab.add(self, text=str(first_name) + second_name)

    def create(self, first_name):
        self.label_brand_name = Label(self, text="NAME", font='TkDefaultFont 14 bold')
        self.brand_name = Label(self, font='TkDefaultFont 14 bold')
        self.entry_brand_name = ttk.Entry(self, font='TkDefaultFont 14 bold')

        self.label_phone_name = Label(self, text="NAME", font='TkDefaultFont 14 bold')
        self.phone_name = Label(self, font='TkDefaultFont 14 bold')
        self.label_phone_brand_id = Label(self, text="BRAND_ID", font='TkDefaultFont 14 bold')
        self.label_phone_brand_name = Label(self, font='TkDefaultFont 14 bold')
        self.label_phone_creation_year = Label(self, text="CREATION_YEAR", font='TkDefaultFont 14 bold')
        self.label_phone_quantity = Label(self, text="QUANTITY", font='TkDefaultFont 14 bold')
        self.label_phone_price = Label(self, text="PRICE", font='TkDefaultFont 14 bold')
        self.entry_phone_name = ttk.Entry(self, font='TkDefaultFont 14 bold')
        self.entry_phone_brand_id = ttk.Entry(self, font='TkDefaultFont 14 bold')
        self.entry_phone_creation_year = ttk.Entry(self, font='TkDefaultFont 14 bold')
        self.entry_phone_quantity = ttk.Entry(self, font='TkDefaultFont 14 bold')
        self.entry_phone_price = ttk.Entry(self, font='TkDefaultFont 14 bold')

        self.label_client_order_client_id = Label(self, text="CLIENT_ID", font='TkDefaultFont 14 bold')
        self.label_client_order_phone_id = Label(self, text="PHONE_ID", font='TkDefaultFont 14 bold')
        self.label_client_order_price = Label(self, text="PRICE = ", font='TkDefaultFont 14 bold')
        self.label_client_order_phone_name = Label(self, font='TkDefaultFont 14 bold')
        self.label_client_order_client_name = Label(self, font='TkDefaultFont 14 bold')
        self.client_order_price = Label(self, text="0", font='TkDefaultFont 14 bold')
        self.entry_client_order_phone_id = ttk.Entry(self, font='TkDefaultFont 14 bold')
        self.entry_client_order_client_id = ttk.Entry(self, font='TkDefaultFont 14 bold')

        self.label_client_name = Label(self, text="NAME", font='TkDefaultFont 14 bold')
        self.entry_client_name = ttk.Entry(self, font='TkDefaultFont 14 bold')

        if first_name == "brand":
            self.label_brand_name.grid(row=0, column=0)
            self.brand_name.grid(row=0, column=2)

            self.entry_brand_name.grid(row=0, column=1)

            self.entry_brand_name.insert(0, self.values[1])

            self.entry_brand_name.bind('<KeyRelease>', self.check_brand_name)

            btn_apply = ttk.Button(self, text="Изменить", style='Bold.TButton', command=lambda: self.init(first_name))
            btn_apply.grid(row=1, column=0)
        elif first_name == "phone":
            # self.label_phone_img = Label(self, text="IMG", font=('TkDefaultFont 14 bold'))

            self.label_phone_name.grid(row=0, column=0)
            self.phone_name.grid(row=0, column=2)
            self.label_phone_brand_id.grid(row=1, column=0)
            self.label_phone_brand_name.grid(row=1, column=2)
            self.label_phone_creation_year.grid(row=2, column=0)
            self.label_phone_quantity.grid(row=3, column=0)
            self.label_phone_price.grid(row=4, column=0)
            # self.label_phone_img.grid(row=5, column=0)

            # self.img_phone_img = ttk.Entry(self, font=('TkDefaultFont 14 bold'))

            self.entry_phone_brand_id.bind('<KeyRelease>', self.check_brand)
            self.entry_phone_name.bind('<KeyRelease>', self.check_phone_name)

            self.entry_phone_name.grid(row=0, column=1)
            self.entry_phone_brand_id.grid(row=1, column=1)
            self.entry_phone_creation_year.grid(row=2, column=1)
            self.entry_phone_quantity.grid(row=3, column=1)
            self.entry_phone_price.grid(row=4, column=1)
            # self.img_phone_img.grid(row=5, column=1)

            self.entry_phone_name.insert(0, self.values[1])
            self.entry_phone_brand_id.insert(0, self.values[2])
            self.entry_phone_creation_year.insert(0, self.values[3])
            self.entry_phone_quantity.insert(0, self.values[4])
            self.entry_phone_price.insert(0, self.values[5])

            btn_apply = ttk.Button(self, text="Изменить", style='Bold.TButton', command=lambda: self.init(first_name))
            btn_apply.grid(row=5, column=0)
        elif first_name == "client_order":

            self.label_client_order_client_id.grid(row=0, column=0)
            self.label_client_order_client_name.grid(row=0, column=2)
            self.label_client_order_phone_id.grid(row=1, column=0)
            self.label_client_order_phone_name.grid(row=1, column=2)
            self.label_client_order_price.grid(row=2, column=0)
            self.client_order_price.grid(row=2, column=1)

            self.entry_client_order_client_id.bind('<KeyRelease>', self.check_client_name)
            self.entry_client_order_phone_id.bind('<KeyRelease>', self.check_phone)

            self.entry_client_order_client_id.grid(row=0, column=1)
            self.entry_client_order_phone_id.grid(row=1, column=1)

            self.entry_client_order_client_id.insert(0, self.values[1])
            self.entry_client_order_phone_id.insert(0, self.values[2])
            self.client_order_price['text'] = self.values[3]

            btn_apply = ttk.Button(self, text="Изменить", style='Bold.TButton', command=lambda: self.init(first_name))
            btn_apply.grid(row=3, column=0)
        elif first_name == "client":
            self.label_client_name.grid(row=0, column=0)

            self.entry_client_name.grid(row=0, column=1)

            self.entry_client_name.insert(0, self.values[1])

            btn_apply = ttk.Button(self, text="Изменить", style='Bold.TButton', command=lambda: self.init(first_name))
            btn_apply.grid(row=1, column=0)

    def init(self, first_name):
        if first_name == "brand":
            if db.check_brand(self.entry_brand_name.get(), self.brand_name):
                db.update_brand(self.values[0], self.entry_brand_name.get())
        elif first_name == "phone":
            db.update_phone(self.values[0], self.entry_phone_name.get(), int(self.entry_phone_brand_id.get()),
                            int(self.entry_phone_creation_year.get()), int(self.entry_phone_quantity.get()),
                            float(self.entry_phone_price.get()))
        elif first_name == "client_order":
            if db.get_phone_price_quantity(self.entry_client_order_phone_id.get(), self.client_order_price):
                db.update_client_order(self.values[0], int(self.entry_client_order_client_id.get()),
                                       int(self.entry_client_order_phone_id.get()),
                                       float(self.client_order_price['text']))
            root.after(100, db.get_phone_price_quantity,
                       self.entry_client_order_phone_id.get(), self.client_order_price)
            root.after(100, db.get_phone_full_name,
                       self.entry_client_order_phone_id.get(), self.label_client_order_phone_name)
        elif first_name == "client":
            db.update_client(self.values[0], self.entry_client_name.get())


class DataBase:
    def __init__(self):
        self.db = None

    def connect(self):
        db_config = python_mysql_dbconfig.read_db_config(filename="files/config.ini")

        try:
            self.db = MySQLConnection(**db_config)
            print("Соединение произошло успешно!")
        except Error as e:
            main_app.status_bar['text'] = f"Ошибка соединения с сервером MySQL или '{e}'!"

        return self.db

    def get_brand_name(self, brand_id, label_output):
        self.connect()

        try:
            brand_id = int(brand_id)
        except ValueError as value_err:
            main_app.status_bar['text'] = value_err

        query = """SELECT brand.name FROM brand WHERE brand.id = %d"""
        args = brand_id

        try:
            cursor = self.db.cursor()
            cursor.execute(query % args)
            output = cursor.fetchall()
            print(output)
            cursor.close()
            self.db.close()
            if not output:
                label_output['text'] = 'Отсутствует'
            else:
                label_output['text'] = output
        except TypeError as type_err:
            main_app.status_bar['text'] = type_err
            label_output['text'] = ''

    def check_brand(self, brand_name, label_output):
        self.connect()

        query = """SELECT brand.name FROM brand WHERE brand.name = '%s'"""
        args = brand_name

        ret: bool

        try:
            cursor = self.db.cursor()
            cursor.execute(query % args)
            output = cursor.fetchall()
            print(output)
            cursor.close()
            self.db.close()
            if not output:
                label_output['text'] = ''
                ret = True
            else:
                label_output['text'] = 'Это имя уже занято'
                ret = False
        except TypeError as type_err:
            main_app.status_bar['text'] = type_err
            label_output['text'] = ''
            ret = False

        return ret

    def insert_brand(self, name):
        self.connect()

        query = """INSERT INTO brand(name) VALUES ('%s')"""
        args = name

        try:
            cursor = self.db.cursor()
            cursor.execute(query % args)

            if cursor.lastrowid:
                print('last insert id', cursor.lastrowid)
            else:
                print('last insert id not found')

            self.db.commit()
            cursor.close()
            self.db.close()
        except Error as error:
            main_app.status_bar['text'] = error

    def update_brand(self, ID, name):
        self.connect()

        query = f"UPDATE brand SET name = '{name}' WHERE id = {ID}"

        try:
            cursor = self.db.cursor()
            cursor.execute(query)

            self.db.commit()
            cursor.close()
            self.db.close()
        except Error as error:
            main_app.status_bar['text'] = error

    def get_phone_price_quantity(self, phone_id, label_output):
        self.connect()

        try:
            phone_id = int(phone_id)
        except ValueError as value_err:
            main_app.status_bar['text'] = value_err

        query1 = """SELECT phone.price FROM phone WHERE phone.id = %d"""
        args1 = phone_id

        query2 = """SELECT quantity FROM phone WHERE id = %d"""
        args2 = phone_id

        ret = False

        try:
            cursor = self.db.cursor()
            cursor.execute(query1 % args1)
            price = cursor.fetchall()

            for item in price:
                price = list(item)

            print(price)

            cursor.execute(query2 % args2)
            quantity = cursor.fetchall()

            for item in quantity:
                quantity = list(item)

            cursor.close()
            self.db.close()
            if not price:
                label_output['text'] = '0'
                ret = False
            elif price and quantity[0] > 0:
                label_output['text'] = price[0]
                ret = True
            elif price and quantity[0] <= 0:
                label_output['text'] = 'Отсутствует на складе!'
                ret = False
        except TypeError as type_err:
            main_app.status_bar['text'] = type_err
            label_output['text'] = '0'
            ret = False

        return ret

    def get_phone_full_name(self, phone_id, label_output):
        self.connect()

        try:
            phone_id = int(phone_id)
        except ValueError as value_err:
            main_app.status_bar['text'] = value_err

        query = """SELECT brand.name AS brand, phone.name AS phone
        FROM phone JOIN brand ON phone.brand_id = brand.id
        WHERE phone.id = %d"""
        args = phone_id

        try:
            cursor = self.db.cursor()
            cursor.execute(query % args)
            output = cursor.fetchall()
            for item in output:
                output = list(item)
            print(output)
            cursor.close()
            self.db.close()
            if not output:
                label_output['text'] = 'Нет в списке!'
            else:
                label_output['text'] = output[0] + " " + output[1]
        except TypeError as type_err:
            main_app.status_bar['text'] = type_err
            label_output['text'] = ''

    def check_phone(self, phone_name, brand_id, label_output):
        self.connect()

        try:
            brand_id = int(brand_id)
        except ValueError as value_err:
            main_app.status_bar['text'] = value_err

        query1 = """SELECT phone.name, brand.name
        FROM phone JOIN brand ON phone.brand_id = brand.id
        WHERE phone.name = '%s'"""
        args1 = phone_name

        query2 = """SELECT brand.name
        FROM brand
        WHERE brand.id = %d"""
        args2 = brand_id

        ret = False

        try:
            cursor = self.db.cursor()
            cursor.execute(query1 % args1)
            output1 = cursor.fetchall()
            cursor.execute(query2 % args2)
            output2 = cursor.fetchall()

            l1 = list()
            l2 = list()
            for item in output1:
                item = list(item)
                for i in item:
                    l1.append(i)

            for item in output2:
                item = list(item)
                l2.append(item[0])

            print(l1)
            print(l2)

            cursor.close()
            self.db.close()

            if not output1:
                label_output['text'] = ''
                ret = True
            elif l1[1] == l2[0]:
                label_output['text'] = 'Это имя уже занято'
                ret = False
            elif l1[1] != l2[0]:
                label_output['text'] = ''
                ret = True
        except TypeError as type_err:
            main_app.status_bar['text'] = type_err
            label_output['text'] = ''
            ret = False

        return ret

    def insert_phone(self, name: str, brand_id: int, creation_year: int, quantity: int, price: float, img=''):
        self.connect()

        query = """INSERT INTO phone(name, brand_id, creation_year, quantity, price, img)
        VALUES ('%s', %d, %d, %d, %f, '%s')"""
        args = (name, brand_id, creation_year, quantity, price, img)

        try:
            cursor = self.db.cursor()
            cursor.execute(query % args)

            if cursor.lastrowid:
                print('last insert id', cursor.lastrowid)
            else:
                print('last insert id not found')

            self.db.commit()
            cursor.close()
            self.db.close()
        except Error as error:
            main_app.status_bar['text'] = error

    def buy_phone(self, phone_id):
        self.connect()

        query = """UPDATE phone
        SET quantity = quantity - 1
        WHERE id = %d AND quantity > 0"""
        args = phone_id

        try:
            cursor = self.db.cursor()
            cursor.execute(query % args)

            self.db.commit()
            cursor.close()
            self.db.close()
        except Error as error:
            main_app.status_bar['text'] = error

    def update_phone(self, ID, name, brand_id, creation_year, quantity, price):
        self.connect()

        query = f"UPDATE phone SET name = '{name}', " \
                f"brand_id = {brand_id}, " \
                f"creation_year = {creation_year}, " \
                f"quantity = {quantity}, " \
                f"price = {price} " \
                f"WHERE id = {ID}"

        try:
            cursor = self.db.cursor()
            cursor.execute(query)

            self.db.commit()
            cursor.close()
            self.db.close()
        except Error as error:
            main_app.status_bar['text'] = error

    def insert_client_order(self, client_id, phone_id, price):
        self.connect()

        query = """INSERT INTO client_order(client_id, phone_id, price) VALUES (%s, %s, %s)"""
        args = (client_id, phone_id, price)

        try:
            cursor = self.db.cursor()
            cursor.execute(query % args)

            if cursor.lastrowid:
                print('last insert id', cursor.lastrowid)
            else:
                print('last insert id not found')

            self.db.commit()
            cursor.close()
            self.db.close()
        except Error as error:
            main_app.status_bar['text'] = error

    def update_client_order(self, ID, client_id, phone_id, price):
        self.connect()

        query = f"UPDATE client_order SET client_id = {client_id}, " \
                f"phone_id = {phone_id}, " \
                f"price = {price} " \
                f"WHERE id = {ID}"

        try:
            cursor = self.db.cursor()
            cursor.execute(query)

            self.db.commit()
            cursor.close()
            self.db.close()
        except Error as error:
            main_app.status_bar['text'] = error

    def get_client_name(self, client_id, label_output):
        self.connect()

        try:
            client_id = int(client_id)
        except ValueError as value_err:
            main_app.status_bar['text'] = value_err

        query = """SELECT client.name FROM client WHERE client.id = %d"""
        args = client_id

        try:
            cursor = self.db.cursor()
            cursor.execute(query % args)
            output = cursor.fetchall()
            print(output)
            cursor.close()
            self.db.close()
            if not output:
                label_output['text'] = ''
            else:
                label_output['text'] = output
        except TypeError as type_err:
            main_app.status_bar['text'] = type_err
            label_output['text'] = ''

    def insert_client(self, name):
        self.connect()

        query = """INSERT INTO client(name) VALUES ('%s')"""
        args = name

        try:
            cursor = self.db.cursor()
            cursor.execute(query % args)

            if cursor.lastrowid:
                print('last insert id', cursor.lastrowid)
            else:
                print('last insert id not found')

            self.db.commit()
            cursor.close()
            self.db.close()
        except Error as error:
            main_app.status_bar['text'] = error

    def update_client(self, ID, name):
        self.connect()

        query = f"UPDATE client SET name = '{name}' WHERE id = {ID}"

        try:
            cursor = self.db.cursor()
            cursor.execute(query)

            self.db.commit()
            cursor.close()
            self.db.close()
        except Error as error:
            main_app.status_bar['text'] = error


root = Tk()
root.title("DMobile")
main_app = MainWin(root)
WelcomeTable(main_app.tab_control, "Welcome")
db = DataBase()
root.geometry("800x400+400+150")
root.mainloop()
